﻿create table USERS(
	USER_ID integer primary key,
	USER_NAME varchar(20) unique,
	PASSWORD varchar(70),
	NAME varchar(20),
	SURNAME varchar(20),
	BALANCE integer,
	EMAIL varchar(30)
);

create table TEAM(
	TEAM_ID integer primary key,
	TEAM_NAME varchar(20) unique
);

create table MATCH(
	MATCH_ID integer primary key,
	HOME integer references TEAM(TEAM_ID),
	AWAY integer references TEAM(TEAM_ID),
	MATCH_DATE datetime,
	ODDS_H float,
	ODDS_A float,
	ODDS_X float,
	GOALS_H integer,
	GOALS_A integer
);

create table TICKET(
	TICKET_ID integer primary key,
	USER_ID integer references USERS(USER_ID),
	MONEY integer
);

create table MATCH_TICKET(
	TICKET_ID integer references TICKET(TICKET_ID),
	MATCH_ID integer references MATCH(MATCH_ID),
	BET_TYPE integer,
	constraint pk_match_ticket primary key (TICKET_ID, MATCH_ID)
);