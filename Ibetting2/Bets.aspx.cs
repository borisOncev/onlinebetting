﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Diagnostics;

namespace Ibetting2
{
    public partial class Bets : System.Web.UI.Page
    {
        private static List<TMatch> match = new List<TMatch>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["userid"] == null)
                {
                    Response.Redirect("Default.aspx");
                }
                else
                    if (!((string)Session["role"]).Equals("user"))
                    {
                        Response.Redirect("AdminTickets.aspx");
                    }
                    else
                    {
                        Label1.Text += (string)Session["username"];
                    }

                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["betConnection"].ConnectionString;
                conn.Open();
                List<Match> matchesNoClosed = DB.getAllMatches(conn);
                GridView1.DataSource = matchesNoClosed;
                GridView1.DataBind();
                conn.Close();
                Label6.Text = "Balance: " + (int)Session["balance"];
            }
            
        }

        protected void Purches(Object sender,
                           EventArgs e)
        {
            if (GridView2.Rows.Count == 0)
            {
                Label4.Text = "Ticket must not be empty";
                return;
            }
            else
            {
                Label4.Text = "";
            }

            int balance = (int)Session["balance"];
            if (balance<Int32.Parse(MoneyBox.Text))
            {
                Label4.Text = "You don't have that much money";
                return;
            }
            else
            {
                Label4.Text = "";
            }

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["betConnection"].ConnectionString;
            conn.Open();
            Debug.WriteLine(MoneyBox.Text);
            int money = Int32.Parse(MoneyBox.Text);
            int User_ID = (int)Session["userid"];
            float multiplier = 1;
            foreach (TMatch m in match)
            {
                multiplier *= m.odds;
            }

            if (multiplier * money > 600000)
            {
                Label4.Text = "You exceed the maximum possible winning of 600000";
                return;
            }
            else
            {
                Label4.Text = "";
            }

            DateTime thisDay = DateTime.Today;
            int ticketId = DB.insertTicket(User_ID, money, multiplier, thisDay, conn);

            foreach(TMatch m in match)
            {
                DB.addMatchToTicket(ticketId, m.matchID, m.pick, conn);
            }

            Session["balance"] = balance - money;
            Label6.Text = "Balance: " + (int)Session["balance"];
            DB.updateBalance(conn, balance - money, (string)Session["username"]);
            conn.Close();
            match.Clear();
            GridView2.DataSource = match;
            GridView2.DataBind();
            Server.Transfer("MyTicket.aspx", true);
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (GridView2.Rows.Count == 10)
            {
                Label5.Text = "Maximum number of games per ticket is 10";
                return;
            }
            else
            {
                Label5.Text = "";
            }
            if (e.CommandName == "GetInfo")
            {
                // Retrieve the row index stored in the 
                // CommandArgument property.
                int index = Convert.ToInt32(e.CommandArgument);

                // Retrieve the row that contains the button 
                // from the Rows collection.
                GridViewRow row = GridView1.Rows[index];
                int M_ID = Int32.Parse(row.Cells[0].Text);

                foreach (GridViewRow gwr in GridView2.Rows)
                {
                    int gameInTicketID = Int32.Parse(gwr.Cells[0].Text);

                    if (gameInTicketID == M_ID)
                    {
                        Label5.Text = "Game is already in ticket";
                        return;
                    }
                    else
                    {
                        Label5.Text = "";
                    }
                }
                DateTime start = DateTime.Parse(row.Cells[1].Text);
                string home = row.Cells[2].Text;
                string away = row.Cells[6].Text;

                TextBox tmp = (TextBox)row.Cells[7].FindControl("TextBoxType");
                if (string.IsNullOrEmpty(tmp.Text))
                {

                }
                int p = 1;
                float odds = (float)Double.Parse(row.Cells[4].Text);
                string choice = tmp.Text;
                Debug.WriteLine(choice);
                if (choice == "X"|| choice == "0")
                {
                    p = 0;
                    odds = (float)Double.Parse(row.Cells[4].Text);
                }
                else if (choice == "2")
                {
                    p = 2;
                    odds = (float)Double.Parse(row.Cells[5].Text);
                }
                else
                {
                    odds = (float)Double.Parse(row.Cells[3].Text);
                }

                bool have = false;
                foreach (TMatch m in match)
                {
                    if (m.matchID == M_ID)
                    {
                        have = true;
                        break;
                    }
                }
                if (!have)
                    match.Add(new TMatch(M_ID, home, away, start, p, odds));

                GridView2.DataSource = match;
                GridView2.DataBind();

            }
            else
            {
                Debug.WriteLine("purchese asd asd");
            }
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("Default.aspx");
        }
    }
}