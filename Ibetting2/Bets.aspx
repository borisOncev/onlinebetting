﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Bets.aspx.cs" Inherits="Ibetting2.Bets" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand " href="#">iBetting</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" style="margin-top:10px">
          <ul class="nav navbar-nav">
      <li>&nbsp&nbsp&nbsp&nbsp&nbsp</li>
            <li class="active"><a href="Bets.aspx">Bets</a></li>
            <li><a href="MyTicket.aspx">My Tickets</a></li>            
          </ul>   
          <ul class="nav navbar-nav navbar-right">
              <li><asp:Label ID="Label2" runat="server" Text="Logged in as: " ></asp:Label></li>
              <li>&nbsp</li>
              <li><asp:Label ID="Label1" runat="server" Text="" ForeColor="#0033CC" ></asp:Label></li>
              <li>&nbsp&nbsp&nbsp</li>
             <li><asp:Button ID="Button4" runat="server" Text="Logout" CssClass="btn btn-success" OnClick="Button4_Click" /></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
        <h1>Place bets: </h1>

    <div class="tabela">
        <table style="width:100%;">
            <tr>
                <td width="60%" style="border-style: solid; border-width: thin">
                    <asp:GridView ID="GridView1" runat="server" HorizontalAlign="Center"
                         AutoGenerateColumns="False" onrowcommand="GridView1_RowCommand" 
                        style="text-align: center" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                        <Columns>
                            <asp:BoundField DataField="matchID" HeaderText="Sifra" />
                            <asp:BoundField DataField="startTime" HeaderText="Start Time" />
                            <asp:BoundField DataField="home" HeaderText="Home" />
                            <asp:BoundField DataField="odds_h" HeaderText="1" />
                            <asp:BoundField DataField="odds_x" HeaderText="X" />
                            <asp:BoundField DataField="odds_a" HeaderText="2" />
                            <asp:BoundField DataField="away" HeaderText="Away" />
                             <asp:TemplateField HeaderText="Type"><ItemTemplate><asp:TextBox ID="TextBoxType" runat="server"></asp:TextBox></ItemTemplate></asp:TemplateField>
                            <asp:TemplateField HeaderText="Add to Ticket"><ItemTemplate>
                                <asp:Button runat="server" Text="Add to Ticket" CommandName="GetInfo" 
                    CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" UseSubmitBehavior="False" /></ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                    </asp:GridView>
                </td>
                <td style="border-style: solid; border-width: thin">
                    <asp:GridView ID="GridView2" runat="server" HorizontalAlign="Center" 
                        AutoGenerateColumns="False" style="text-align: center" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                        <Columns>
                            <asp:BoundField DataField="matchID" HeaderText="Sifra" />
                            <asp:BoundField DataField="startTime" HeaderText="Start Time" />
                            <asp:BoundField DataField="home" HeaderText="Home" />
                            <asp:BoundField DataField="away" HeaderText="Away" />
                            <asp:BoundField DataField="pick" HeaderText="Type" />
                            <asp:BoundField DataField="odds" HeaderText="Odds" />
                            
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td style="text-align: center" rowspan="2">
                    <asp:Label ID="Label5" runat="server" ForeColor="Red" style="text-align: center; font-size: large"></asp:Label>
                </td>
                <td class="text-left">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                    <asp:Label ID="Label6" runat="server" Text="Balance:"></asp:Label>
                           
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                    <asp:Label ID="Label4" runat="server" ForeColor="Red"></asp:Label>
                    <br />
                    <asp:Label ID="Money" runat="server" Text="Money: " height="26px"></asp:Label>
                    <asp:TextBox ID="MoneyBox" runat="server" ValidationGroup="money">50</asp:TextBox>
                            
                    <asp:Button runat="server" Text="Purchase Ticket" CommandName="Purchese" 
                    CommandArgument="<%# 2 %>" UseSubmitBehavior="False" OnClick="Purches" ValidationGroup="money" ID="Button5" />
                           
                    <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="MoneyBox" Display="Dynamic" ErrorMessage="Enter money" ForeColor="Red" ValidationGroup="money"></asp:RequiredFieldValidator>
                    <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="MoneyBox" Display="Dynamic" ErrorMessage="Must be between 20 and 10000" ForeColor="Red" MaximumValue="10000" MinimumValue="20" Type="Integer" ValidationGroup="money"></asp:RangeValidator>
                    <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="MoneyBox" ErrorMessage="Enter only digits" ForeColor="Red" ValidationExpression="[0-9]*" ValidationGroup="money"></asp:RegularExpressionValidator>
                           
                </td>
            </tr>
        </table>
        </div>
</asp:Content>
