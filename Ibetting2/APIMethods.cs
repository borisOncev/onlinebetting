﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml.Linq;

namespace Ibetting2
{
    public class APIMethods
    {
        public static List<Match> getMatchesForDate(DateTime date)
        {
            //string apiKey = "x6ruycfzc3e9m46rpq5pygha";//filip
            string apiKey = "yxzmmz9vsc8gujfus6bme5ab";//goran
            CultureInfo enUS = CultureInfo.CreateSpecificCulture("en-US");
            string dateFormat = String.Format(enUS, "{0 :yyyy/MM/dd}", date);
            WebRequest request = WebRequest.Create("http://api.sportradar.us/soccer-t2/na/matches/" + dateFormat + "/boxscore.xml?api_key=" + apiKey);
            request.Method = "GET";
            request.ContentType = "text/xml";
            List<Match> result = new List<Match>();

            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            XDocument docFinal = XDocument.Parse(reader.ReadToEnd());
            XNamespace nameSpace = docFinal.Root.Attribute("xmlns").Value;
            XElement matches = docFinal.Root.Element(nameSpace + "matches");
            if (matches.HasElements)
            {
                for (int i = 0; i < matches.Elements(nameSpace + "match").Count(); i++)
                {
                    XElement tmp=matches.Elements(nameSpace + "match").ElementAt(i);
                    if (tmp.Attribute("status").Value.Equals("scheduled"))
                    {
                        result.Add(new Match(0,tmp.Element(nameSpace + "home").Attribute("name").Value, tmp.Element(nameSpace + "away").Attribute("name").Value, 
                            0, 0, Convert.ToDateTime(tmp.Attribute("scheduled").Value), "scheduled"));
                    }
                    else
                    {
                        result.Add(new Match(0, tmp.Element(nameSpace + "home").Attribute("name").Value, tmp.Element(nameSpace + "away").Attribute("name").Value,
                            Int32.Parse(tmp.Element(nameSpace + "home").Attribute("score").Value), Int32.Parse(tmp.Element(nameSpace + "away").Attribute("score").Value),
                            Convert.ToDateTime(tmp.Attribute("scheduled").Value), tmp.Attribute("status").Value));
                    }

                }
            }
            //DateTime t = Convert.ToDateTime(matches.Elements(nameSpace + "match").ElementAt(0).Attribute("scheduled").Value);
            //Label1.Text = matches.Elements(nameSpace + "match").ElementAt(0).Attribute("scheduled").Value;
            //Label1.Text += " " + matches.Elements(nameSpace + "match").ElementAt(0).Attribute("status").Value;
            //Label1.Text += " " + matches.Elements(nameSpace + "match").ElementAt(0).Element(nameSpace + "home").Attribute("name").Value;
            //Label1.Text += " " + matches.Elements(nameSpace + "match").ElementAt(0).Element(nameSpace + "home").Attribute("score").Value;
            //Label1.Text += " " + matches.Elements(nameSpace + "match").ElementAt(0).Element(nameSpace + "away").Attribute("score").Value;
            //Label1.Text += " " + matches.Elements(nameSpace + "match").ElementAt(0).Element(nameSpace + "away").Attribute("name").Value;
            return result;
        }
    }
}