﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminTickets.aspx.cs" Inherits="Ibetting2.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand " href="#">iBetting</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" style="margin-top:10px">
          <ul class="nav navbar-nav">
      <li>&nbsp&nbsp&nbsp&nbsp&nbsp</li>
            <li class="active"><a href="AdminTickets.aspx">All Tickets</a></li>
            <li><a href="AdminStats.aspx">Statistics</a></li>
            <li ><a href="AdminAddGames.aspx">Add Games</a></li>
            
          </ul>   
          <ul class="nav navbar-nav navbar-right">
              <li><asp:Label ID="Label2" runat="server" Text="Logged in as: " ></asp:Label></li>
              <li>&nbsp</li>
              <li><asp:Label ID="Label1" runat="server" Text="" ForeColor="#0033CC" ></asp:Label></li>
              <li>&nbsp&nbsp&nbsp</li>
             <li class="active"><asp:Button ID="Button4" runat="server" Text="Logout" CssClass="btn btn-success" OnClick="Button4_Click" /></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
        <h1>All Tickets: </h1>


            <div class="tabela">
        <table style="width:100%;">
            <tr>
                <td width="35%" style="border-style: solid; border-width: thin">
                    <div style="margin: 10px auto 10px auto; display: inline-block;" > 
                    <asp:Calendar ID="Calendar1" runat="server" BackColor="White" BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="#003399" Height="200px" Width="220px">
                        <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                        <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                        <OtherMonthDayStyle ForeColor="#999999" />
                        <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                        <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                        <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                        <WeekendDayStyle BackColor="#CCCCFF" />
                    </asp:Calendar>
                        </div>
                    <br />
                    <br />
                    <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Show Tickets" />
                    <br />
                    <br />
                </td>
                <td style="border-style: solid; border-width: thin">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="border-style: solid; border-width: thin">
                    <asp:GridView ID="GridView1" runat="server" HorizontalAlign="Center" AutoGenerateColumns="False" OnRowCommand="GridView1_RowCommand" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                        <Columns>
                            <asp:BoundField DataField="ticketID" HeaderText="TICKETID" />
                            <asp:BoundField DataField="balance" HeaderText="BALANCE" />
                            <asp:BoundField DataField="multiplier" HeaderText="MULTIPLIER" />
                            <asp:TemplateField HeaderText="SHOW"><ItemTemplate>
                                <asp:Button runat="server" Text="Get Info" CommandName="GetInfo" 
                    CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" UseSubmitBehavior="False" /></ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                    </asp:GridView>
                </td>
                <td style="border-style: solid; border-width: thin">
                    <asp:GridView ID="GridView2" runat="server" HorizontalAlign="Center" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                        <Columns>
                            <asp:BoundField DataField="startTime" HeaderText="Time" />
                            <asp:BoundField DataField="home" HeaderText="Home" />
                            <asp:BoundField DataField="homeGoals" HeaderText="HGoals" />
                            <asp:BoundField DataField="awayGoals" HeaderText="AGoals" />
                            <asp:BoundField DataField="away" HeaderText="Away" />
                            <asp:BoundField DataField="pick" HeaderText="Pick" />
                            <asp:BoundField DataField="status" HeaderText="Status" />
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                    </asp:GridView>
                    <br />
                    <asp:Label ID="Label5" runat="server" Text="Money:"></asp:Label>
                    <br />
                    <asp:Label ID="Label6" runat="server" Text="Multiplier:"></asp:Label>
                    <br />
                    <asp:Label ID="Label7" runat="server" Text="Possible Win:"></asp:Label>
                    <br />
                    <asp:Label ID="Label4" runat="server" Text="Status:"></asp:Label>
                </td>
            </tr>
        </table>
        </div>
</asp:Content>
