﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Timers;
using System.Diagnostics;
using System.Configuration;

namespace Ibetting2
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection conn;
            if (!Page.IsPostBack)
            {
                conn = new SqlConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["betConnection"].ConnectionString;
                conn.Open();
                List<Match> matchesNoClosed = DB.selectDefaultPageMatch(conn);
                GridView1.DataSource = matchesNoClosed;
                GridView1.DataBind();
                conn.Close();
            }

        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["betConnection"].ConnectionString;
            conn.Open();
            List<Match> matchesNoClosed = DB.selectDefaultPageMatch(conn);
            GridView1.DataSource = matchesNoClosed;
            GridView1.DataBind();
            Debug.WriteLine("Timer raboti");
            conn.Close();
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Redirect("Register.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["betConnection"].ConnectionString;
            conn.Open();
            User user = DB.getUser(conn, TextBox1.Text);
            conn.Close();
            if (user != null)
            {
                if (PasswordHash.ValidatePassword(TextBox4.Text, user.password))
                {
                    Session["userid"] = user.userid;
                    Session["username"] = user.username;
                    Session["balance"] = user.balance;
                    Session["role"] = user.role;

                    if (user.role.Equals("user"))
                    {
                        Response.Redirect("MyTicket.aspx");
                    }
                    else
                    {
                        Response.Redirect("AdminTickets.aspx");
                    }
                }
                else
                {
                    Label1.Visible = true;
                }

            }
            else
            {
                Label1.Visible = true;
            }
        }


        

    }
}
