﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace Ibetting2
{
    public partial class MyTicket : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
                if (Session["userid"] == null)
                {
                    Response.Redirect("Default.aspx");
                }
                else
                    if (!((string)Session["role"]).Equals("user"))
                    {
                        Response.Redirect("AdminTickets.aspx");
                    }
                    else
                    {
                        Label1.Text += (string)Session["username"];
                    }

                //Session["userid"] = 7;
                SqlConnection conn = null;
                
                    conn = new SqlConnection();
                    conn.ConnectionString = ConfigurationManager.ConnectionStrings["betConnection"].ConnectionString;
                    conn.Open();
                    List<Ticket> tickets = DB.getTickets((int)Session["userid"], conn);
                    

                    GridView1.DataSource = tickets;
                    GridView1.DataBind();
                    conn.Close();
            
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "GetInfo")
            {
                // Retrieve the row index stored in the 
                // CommandArgument property.
                int index = Convert.ToInt32(e.CommandArgument);

                // Retrieve the row that contains the button 
                // from the Rows collection.
                GridViewRow row = GridView1.Rows[index];
                int ticketID = Int32.Parse(row.Cells[0].Text);
                List<Match> match = new List<Match>();
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["betConnection"].ConnectionString;
                conn.Open();
                
                
                match = DB.getMatchesForTicket(conn, ticketID);

                GridView2.DataSource = match;
                GridView2.DataBind();
                Label5.Text = "Money: " + row.Cells[1].Text;
                Label6.Text = "Multiplier: " +row.Cells[2].Text;
                Label7.Text = "Possible Win: " +float.Parse(row.Cells[1].Text) * float.Parse(row.Cells[2].Text);
                bool isWin = true;

                foreach (Match m in match)
                {
                    if(m.status.Equals("closed"))
                    if ((m.homeGoals > m.awayGoals && m.pick == 1) || (m.homeGoals < m.awayGoals && m.pick == 2) || (m.homeGoals == m.awayGoals && m.pick == 0))
                    {

                    }
                    else
                    {
                        isWin = false;
                        break;
                    }
                }
                bool isFinished = true;
                if (isWin)
                {
                    foreach (Match m in match)
                    {
                        if (!m.status.Equals("closed"))
                        {
                            isFinished = false;
                            break;
                        }
                    }
                }

                bool isTicketPaid = DB.isTicketPaid(conn, ticketID);
                conn.Close();
                float posWin=float.Parse(row.Cells[1].Text) * float.Parse(row.Cells[2].Text);
                int balance=(int)Session["balance"];

                if (isWin && isFinished)
                {
                    Label4.Text =  "Status: Ticket Won!";
                    if (!isTicketPaid)
                    {
                        DB.updateBalance(conn, (int)(balance + posWin), (string)Session["username"]);
                        DB.updatePaidStatus(conn, ticketID);
                        Session["balance"] = balance + posWin;
                    }
                }
                if (isWin && !isFinished)
                {
                    Label4.Text = "Status: Ticket still in play";
                }
                if (!isWin)
                {
                    Label4.Text = "Status: Ticket Lost!";
                }
                
                // Add code here to add the item to the shopping cart.
            }
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("Default.aspx");
        }
    }
}