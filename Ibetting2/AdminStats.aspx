﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminStats.aspx.cs" Inherits="Ibetting2.AdminStats" %>
<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 353px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand " href="#">iBetting</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" style="margin-top:10px">
          <ul class="nav navbar-nav">
      <li>&nbsp&nbsp&nbsp&nbsp&nbsp</li>
            <li ><a href="AdminTickets.aspx">All Tickets</a></li>
            <li class="active"><a href="AdminStats.aspx">Statistics</a></li>
            <li ><a href="AdminAddGames.aspx">Add Games</a></li>
            
          </ul>   
          <ul class="nav navbar-nav navbar-right">
              <li><asp:Label ID="Label2" runat="server" Text="Logged in as: " ></asp:Label></li>
              <li>&nbsp</li>
              <li><asp:Label ID="Label1" runat="server" Text="" ForeColor="#0033CC" ></asp:Label></li>
              <li>&nbsp&nbsp&nbsp</li>
             <li class="active"><asp:Button ID="Button4" runat="server" Text="Logout" CssClass="btn btn-success" OnClick="Button4_Click" /></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
        <h1>Statistics: </h1>
            <table style="width:100%;" align="center">
            <tr>
                <td style="border-style: solid; border-width: thin; text-align: center;" class="auto-style1">
                    <div style="display: inline-block; margin-top: 10px; margin-right: inherit; margin-left: auto; margin-bottom: 10px">
                    <asp:Calendar ID="Calendar1" runat="server" BackColor="White" BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="#003399" Height="200px" Width="220px">
                        <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                        <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                        <OtherMonthDayStyle ForeColor="#999999" />
                        <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                        <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                        <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                        <WeekendDayStyle BackColor="#CCCCFF" />
                    </asp:Calendar>
                    </div>
                    <br />
                    <br />
                    <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Show Stats" style="text-align: center" />
                    
                </td>
                <td style="border-style: solid; border-width: thin">
                    
                    <asp:Chart ID="Chart1" runat="server" style="text-align: center">
                        <series>
                            <asp:Series Name="Series1">
                            </asp:Series>
                        </series>
                        <chartareas>
                            <asp:ChartArea Name="ChartArea1">
                            </asp:ChartArea>
                        </chartareas>
                    </asp:Chart>
                    
                    <br />
                    <br />
                    
                </td>
            </tr>
        </table>
</asp:Content>
