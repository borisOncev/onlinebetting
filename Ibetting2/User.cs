﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ibetting2
{
    public class User
    {
        public int userid { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public int balance { get; set; }
        public string role { get; set; }

        public User(int userid, string username, string password, int balance, string role)
        {
            this.userid = userid;
            this.username = username;
            this.password = password;
            this.balance = balance;
            this.role = role;
        }
    }
}