﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ibetting2
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["betConnection"].ConnectionString;
            conn.Open();
            if (DB.isUsernameTaken(conn, TextBox4.Text))
            {
                Label7.Text = "Username already taken";
                Label7.ForeColor = System.Drawing.Color.Red;
                return;
            }
            else
            {
                Label7.Text = "";
            }

            DB.insertUser(TextBox4.Text, TextBox5.Text, TextBox1.Text, TextBox2.Text, 5000, TextBox3.Text, conn);
            conn.Close();
            Response.Redirect("Default.aspx");
        }
    }
}