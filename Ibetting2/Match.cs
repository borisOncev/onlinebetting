﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ibetting2
{
    [Serializable]
    public class Match
    {
        public int matchID { get; set; }
        public string home { get; set; }
        public string away { get; set; }
        public int homeGoals { get; set; }
        public int awayGoals { get; set; }
        public DateTime startTime { get; set; }
        public string status { get; set; }
        public int pick { get; set; }
        public float odds_h { get; set; }
        public float odds_x { get; set; }
        public float odds_a { get; set; }
        
        public Match(int matchID, string home, string away, int homeGoals, int awayGoals, DateTime startTime, string status)
        {
            this.matchID = matchID;
            this.home = home;
            this.away = away;
            this.homeGoals = homeGoals;
            this.awayGoals = awayGoals;
            this.startTime = startTime;
            this.status = status;
        }

        public Match(int matchID, string home, string away,float odds_h,float odds_x,float odds_a, DateTime startTime, string status)
        {
            this.matchID = matchID;
            this.home = home;
            this.away = away;
            this.odds_h = odds_h;
            this.odds_x = odds_h;
            this.odds_a = odds_a;
            this.startTime = startTime;
            this.status = status;
        }

    }
}