﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MyTicket.aspx.cs" Inherits="Ibetting2.MyTicket" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand " href="#">iBetting</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" style="margin-top:10px">
          <ul class="nav navbar-nav">
      <li>&nbsp&nbsp&nbsp&nbsp&nbsp</li>
            <li ><a href="Bets.aspx">Bets</a></li>
            <li class="active"><a href="MyTicket.aspx">My Tickets</a></li>            
          </ul>   
          <ul class="nav navbar-nav navbar-right">
              <li><asp:Label ID="Label2" runat="server" Text="Logged in as: " ></asp:Label></li>
              <li>&nbsp</li>
              <li><asp:Label ID="Label1" runat="server" Text="" ForeColor="#0033CC" ></asp:Label></li>
              <li>&nbsp&nbsp&nbsp</li>
             <li class="active"><asp:Button ID="Button4" runat="server" Text="Logout" CssClass="btn btn-success" OnClick="Button4_Click" /></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
        <h1>My Tickets: </h1>

            <div class="tabela">
        <table style="width:100%;">
            <tr>
                <td style="border-style: solid; border-width: thin; width:45%;">
                    <asp:GridView ID="GridView1" runat="server" HorizontalAlign="Center" 
                        AutoGenerateColumns="False" onrowcommand="GridView1_RowCommand" 
                        style="text-align: center" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                        <Columns>
                            <asp:BoundField DataField="ticketID" HeaderText="Ticket ID" />
                            <asp:BoundField DataField="balance" HeaderText="Balance" />
                            <asp:BoundField DataField="multiplier" HeaderText="Multiplier" />
                            <asp:BoundField DataField="ticketDate" HeaderText="Date" />
                            <asp:TemplateField HeaderText="Get Info"><ItemTemplate>
                                <asp:Button runat="server" Text="Get Info" CommandName="GetInfo" 
                    CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" UseSubmitBehavior="False" /></ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                    </asp:GridView>
                </td>
                <td style="border-style: solid; border-width: thin; width:55%;">
                    <div style="margin-top: 15px; margin-bottom: 10px">
                    <asp:GridView ID="GridView2" runat="server" HorizontalAlign="Center" 
                        AutoGenerateColumns="False" style="text-align: center" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                        <Columns>
                            <asp:BoundField DataField="home" HeaderText="Home" />
                            <asp:BoundField DataField="homeGoals" HeaderText="Goals Home" />
                            <asp:BoundField DataField="awayGoals" HeaderText="Goals Away" />
                            <asp:BoundField DataField="away" HeaderText="Away" />
                            <asp:BoundField DataField="pick" HeaderText="Pick" />
                            <asp:BoundField DataField="status" HeaderText="Status" />
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                    </asp:GridView>
                        <br />
                    <asp:Label ID="Label5" runat="server" Text="Money: "></asp:Label>
                    <br />
                    <asp:Label ID="Label6" runat="server" Text="Multiplier: "></asp:Label>
                    <br />
                    <asp:Label ID="Label7" runat="server" Text="Possible Win: "></asp:Label>
                    <br />
                    <asp:Label ID="Label4" runat="server">Status:</asp:Label>
                        </div>
                </td>
            </tr>
        </table>
        </div>
</asp:Content>
