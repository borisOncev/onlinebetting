﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="Ibetting2.Register" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .auto-style1 {
            text-align: right;
            height: 42px;
        }
        .auto-style2 {
            text-align: left;
            height: 42px;
        }
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand " href="#">iBetting</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" style="margin-top:10px">
          <ul class="nav navbar-nav">
           <li>&nbsp&nbsp&nbsp&nbsp&nbsp</li>
          </ul>   
          
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    
    
    
    
    
    <table cellpadding="10">
        <tr>
            <td colspan="2">
                       <h1>Registration: </h1></td>
        </tr>
        <tr>
            <td width="40%" class="text-right" style="padding: 10px">
                <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label>
            </td>
            <td class="text-left">
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1" ErrorMessage="You can't leave this empty" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="text-right" style="padding: 10px">
                <asp:Label ID="Label2" runat="server" Text="Surname"></asp:Label>
            </td>
            <td class="text-left">
                <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox2" ErrorMessage="You can't leave this empty" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="text-right" style="padding: 10px">
                Date of Birth</td>
            <td class="text-left">
                <asp:TextBox ID="TextBox7" runat="server" placeholder="dd/mm/yyyy"></asp:TextBox>
                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="TextBox7" ErrorMessage="Wrong date format or you have less than 18 years" ForeColor="Red" Operator="LessThanEqual" Type="Date" ValueToCompare="<%# DateTime.Today.AddYears(-18).ToShortDateString() %>"></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="text-right" style="padding: 10px">
                <asp:Label ID="Label3" runat="server" Text="E-mail"></asp:Label>
            </td>
            <td class="text-left">
                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBox3" ErrorMessage="You can't leave this empty" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBox3" ErrorMessage="Enter a valid e-mail address" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="text-right" style="padding: 10px">
                <asp:Label ID="Label4" runat="server" Text="Username"></asp:Label>
            </td>
            <td class="text-left">
                <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBox4" ErrorMessage="You can't leave this empty" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style1" style="padding: 10px">
                <asp:Label ID="Label5" runat="server" Text="Password"></asp:Label>
            </td>
            <td class="auto-style2">
                <asp:TextBox ID="TextBox5" runat="server" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBox5" ErrorMessage="You can't leave this empty" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="text-right" style="padding: 10px">
                <asp:Label ID="Label6" runat="server" Text="Re-enter Password"></asp:Label>
            </td>
            <td class="text-left">
                <asp:TextBox ID="TextBox6" runat="server" TextMode="Password"></asp:TextBox>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TextBox5" ControlToValidate="TextBox6" ErrorMessage="Passwords don't match" ForeColor="Red"></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="text-left">
                <asp:Label ID="Label7" runat="server"></asp:Label>
                <br />
                <br style="text-align: left" />
                <asp:Button ID="Button1" runat="server" Text="Register" OnClick="Button1_Click" />
                <br />
                <br />
                <br />
                <br />
            </td>
        </tr>
    </table>
</asp:Content>
