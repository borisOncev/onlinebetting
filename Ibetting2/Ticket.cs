﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ibetting2
{
    public class Ticket
    {
        public int ticketID { get; set; }
        public int balance { get; set; }
        public float multiplier { get; set; }
        public DateTime ticketDate { get; set; }

        public Ticket(int ticketID, int balance, float multiplier, DateTime ticketDate)
        {
            this.ticketID = ticketID;
            this.balance = balance;
            this.multiplier = multiplier;
            this.ticketDate = ticketDate;
        }
    }
}