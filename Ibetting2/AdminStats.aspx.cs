﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ibetting2
{
    public partial class AdminStats : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
                if (Session["userid"] == null)
                {
                    Response.Redirect("Default.aspx");
                }
                else
                    if (((string)Session["role"]).Equals("user"))
                    {
                        Response.Redirect("MyTicket.aspx");
                    }
                    else
                    {
                        Label1.Text += (string)Session["username"];
                    }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["betConnection"].ConnectionString;
            conn.Open();
            DateTime date = Calendar1.SelectedDate;
            List<Ticket> tickets = DB.getTicketsForDate(date, conn);
            int uplati = 0;
            int isplati = 0;
            int moznaDobivka = 0;

            foreach(Ticket t in tickets)
            {
                uplati += t.balance;
                List<Match> matches = DB.getMatchesForTicket(conn, t.ticketID);
                bool isWin = true;

                foreach (Match m in matches)
                {
                    if (m.status.Equals("closed"))
                        if ((m.homeGoals > m.awayGoals && m.pick == 1) || (m.homeGoals < m.awayGoals && m.pick == 2) || (m.homeGoals == m.awayGoals && m.pick == 0))
                        {

                        }
                        else
                        {
                            isWin = false;
                            break;
                        }
                }
                bool isFinished = true;
                if (isWin)
                {
                    foreach (Match m in matches)
                    {
                        if (!m.status.Equals("closed"))
                        {
                            isFinished = false;
                            break;
                        }
                    }
                }

                if (isWin && isFinished)
                {
                    isplati += (int)(t.balance * t.multiplier);
                }
                if (isWin && !isFinished)
                {
                    moznaDobivka += (int)(t.balance * t.multiplier);
                }
                
            }
            conn.Close();
            List<string> titles = new List<string>();
            titles.Add("Cash in");
            titles.Add("Cash out");
            titles.Add("Unsettled");
            List<int> money = new List<int>();
            money.Add(uplati);
            money.Add(isplati);
            money.Add(moznaDobivka);

            Chart1.Series.Add("stats");
            Chart1.Series["stats"].Points.DataBindXY(titles,money);
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("Default.aspx");
        }
    }
}