﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ibetting2
{
    public partial class AdminAddGames : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!Page.IsPostBack)
            {
                if (Session["userid"] == null)
                {
                    Response.Redirect("Default.aspx");
                }
                else
                    if (((string)Session["role"]).Equals("user"))
                    {
                        Response.Redirect("MyTicket.aspx");
                    }
                    else
                    {
                        Label1.Text += (string)Session["username"];
                    }

                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = ConfigurationManager.ConnectionStrings["betConnection"].ConnectionString;
                conn.Open();
                List<Match> matchesForDate = new List<Match>();
                DateTime tomorrow = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day + 1);
                if (!DB.isDayAdded(conn, tomorrow))
                {
                    matchesForDate = APIMethods.getMatchesForDate(tomorrow);
                }

                if (matchesForDate.Count == 0)
                {
                    Label4.Text = "No matches for tomorrow";
                }
                else
                {
                    GridView1.DataSource = matchesForDate;
                    GridView1.DataBind();
                }

                ViewState["matches"] = matchesForDate;

                conn.Close();
            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            bool filled = true;
            foreach (GridViewRow r in GridView1.Rows)
            {
                if (!filled) break;
                for(int i=2; i<5; i++)
                {
                    TextBox tmp=(TextBox)r.Cells[i].FindControl("TextBox"+(i-1));
                    if(string.IsNullOrEmpty(tmp.Text))
                    {
                        filled = false;
                        break;
                    }
                }
            }

            if (!filled)
            {
                Label4.Text = "Fill all fields!";
                return;
            }

            List<Match> matches=(List<Match>)ViewState["matches"];
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["betConnection"].ConnectionString;
            conn.Open();

            for (int i = 0; i < matches.Count; i++ )
            {
                Match m = matches.ElementAt(i);
                float type1 = float.Parse(((TextBox)GridView1.Rows[i].Cells[2].FindControl("TextBox1")).Text);
                float typeX = float.Parse(((TextBox)GridView1.Rows[i].Cells[3].FindControl("TextBox2")).Text);
                float type2 = float.Parse(((TextBox)GridView1.Rows[i].Cells[4].FindControl("TextBox3")).Text);
                DB.insertMatch(m.home, m.away, m.startTime, type1, type2, typeX, 0, 0, conn);

            }

            Label4.Text = "Adding Succesful!";

            conn.Close();

        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("Default.aspx");
        }
    }
}