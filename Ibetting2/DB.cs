﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;

namespace Ibetting2
{
    public class DB
    {
        public static void insertUser(string user, string pass, string name, string sname, int balance, string email, SqlConnection conn)
        {
            SqlCommand command = new SqlCommand("SELECT NEXT VALUE FOR USR_ID_SEQ", conn);
            int user_ID = 0;
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    user_ID = Convert.ToInt32(reader[0]);
                    Debug.WriteLine(String.Format("NEW USER ID {0}\n",
                        user_ID));
                }
            }

            string hash = PasswordHash.CreateHash(pass);

            SqlCommand insertCommand = new SqlCommand("INSERT INTO USERS (USER_ID, USER_NAME, PASSWORD, NAME, SURNAME, BALANCE, EMAIL, ROLE) VALUES (@0, @1, @2, @3, @4, @5, @6, @7)", conn);
            // Add the parameters.
            insertCommand.Parameters.Add(new SqlParameter("0", user_ID));
            insertCommand.Parameters.Add(new SqlParameter("1", user));
            insertCommand.Parameters.Add(new SqlParameter("2", hash));
            insertCommand.Parameters.Add(new SqlParameter("3", name));
            insertCommand.Parameters.Add(new SqlParameter("4", sname));
            insertCommand.Parameters.Add(new SqlParameter("5", balance));
            insertCommand.Parameters.Add(new SqlParameter("6", email));
            insertCommand.Parameters.Add(new SqlParameter("7", "user"));

            insertCommand.ExecuteNonQuery();
        }

        public static int insertTicket(int user_id,int money,float multiplier,DateTime day, SqlConnection conn)
        {
            SqlCommand command = new SqlCommand("SELECT NEXT VALUE FOR TICKET_ID_SEQ", conn);
            int ticket_id = 0;
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    ticket_id = Convert.ToInt32(reader[0]);
                    Debug.WriteLine(String.Format("NEW TICKET ID {0}\n",
                        ticket_id));
                }
            }

            SqlCommand insertCommand = new SqlCommand("INSERT INTO TICKET (TICKET_ID, USER_ID, MONEY,MULTIPLIER,TICKET_DATE, ISPAID) VALUES (@0, @1, @2, @3, @4, @5)", conn);
            // Add the parameters.
            insertCommand.Parameters.Add(new SqlParameter("0", ticket_id));
            insertCommand.Parameters.Add(new SqlParameter("1", user_id));
            insertCommand.Parameters.Add(new SqlParameter("2", money));
            insertCommand.Parameters.Add(new SqlParameter("3", multiplier));
            insertCommand.Parameters.Add(new SqlParameter("4", day));
            insertCommand.Parameters.Add(new SqlParameter("5", "no"));

            insertCommand.ExecuteNonQuery();
            return ticket_id;
        }

        public static void addMatchToTicket(int ticket_id,int match_id,int type,SqlConnection conn)
        {
            SqlCommand insertCommand = new SqlCommand("INSERT INTO MATCH_TICKET (TICKET_ID, MATCH_ID, BET_TYPE) VALUES (@0, @1, @2)", conn);
            // Add the parameters.
            insertCommand.Parameters.Add(new SqlParameter("0", ticket_id));
            insertCommand.Parameters.Add(new SqlParameter("1", match_id));
            insertCommand.Parameters.Add(new SqlParameter("2", type));

            insertCommand.ExecuteNonQuery();
        }

        private static int insert_Team(string name,SqlConnection conn)
        {
            SqlCommand command = new SqlCommand("SELECT NEXT VALUE FOR TEAM_ID_SEQ", conn);
            int team_id = 0;
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    team_id = Convert.ToInt32(reader[0]);
                    Debug.WriteLine(String.Format("NEW TEAM ID {0}\n",
                        team_id));
                }
            }

            SqlCommand insertCommand = new SqlCommand("INSERT INTO TEAM (TEAM_ID, TEAM_NAME) VALUES (@0, @1)", conn);
            // Add the parameters.
            insertCommand.Parameters.Add(new SqlParameter("0", team_id));
            insertCommand.Parameters.Add(new SqlParameter("1", name));

            insertCommand.ExecuteNonQuery();

            return team_id;
        }

        public static void insertMatch(string home, string away, DateTime date, float odds_h, float odds_a, float odds_x, int goals_h, int goals_a, SqlConnection conn)
        {
            SqlCommand commandTeam = new SqlCommand("SELECT * FROM TEAM", conn);
            SqlDataReader read = commandTeam.ExecuteReader();
            Dictionary<string, int> teams = new Dictionary<string, int>();
            while (read.Read())
            {
                teams.Add(read.GetString(1), read.GetInt32(0));
            }
            int homeID = 0;
            int awayID = 0;

            if(teams.ContainsKey(home))
            {
                homeID = teams[home];
            }
            else
            {
                homeID = insert_Team(home, conn);
            }

            if (teams.ContainsKey(away))
            {
                awayID = teams[away];
            }
            else
            {
                awayID = insert_Team(away, conn);
            }

            SqlCommand command = new SqlCommand("INSERT INTO MATCH (HOME, AWAY, MATCH_DATE, ODDS_H, ODDS_A, ODDS_X, GOALS_H, GOALS_A, STATUS) VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8)", conn);
            command.Parameters.AddWithValue("0", homeID);
            command.Parameters.AddWithValue("1", awayID);
            command.Parameters.AddWithValue("2", date);
            command.Parameters.AddWithValue("3", Math.Round(odds_h, 2, MidpointRounding.AwayFromZero));
            command.Parameters.AddWithValue("4", Math.Round(odds_a, 2, MidpointRounding.AwayFromZero));
            command.Parameters.AddWithValue("5", Math.Round(odds_x, 2, MidpointRounding.AwayFromZero));
            command.Parameters.AddWithValue("6", goals_h);
            command.Parameters.AddWithValue("7", goals_a);
            command.Parameters.AddWithValue("8", "scheduled");
            command.ExecuteNonQuery();

        }

        public static List<Ticket> getTickets(int user_ID, SqlConnection conn)
        {
            SqlCommand command = new SqlCommand("SELECT * FROM TICKET WHERE USER_ID = @0", conn);
            // Add the parameters.
            command.Parameters.Add(new SqlParameter("0", user_ID));
            List<Ticket> tickets = new List<Ticket>();
            /* Get the rows and display on the screen! 
             * This section of the code has the basic code
             * that will display the content from the Database Table
             * on the screen using an SqlDataReader. */
            Debug.WriteLine("sad");
            using (SqlDataReader reader = command.ExecuteReader())
            {
                Debug.WriteLine("TICKET_ID\tUSER_ID\tMONEY\n");
                while (reader.Read())
                {
                    
                    tickets.Add(new Ticket(reader.GetInt32(0), reader.GetInt32(2), (float)reader.GetDouble(3), reader.GetDateTime(4)));
                    //Console.WriteLine(String.Format("{0} \t | {1} \t | {2}",
                    //    reader[0], reader[1], reader[2]));

                }
            }

            return tickets;
        }

        public static List<Match> getAllMatches(SqlConnection conn)
        {
            List<Match> matches = new List<Match>();
            SqlCommand commandMatch = new SqlCommand("select M.MATCH_ID,T1.TEAM_NAME, T2.TEAM_NAME, M.ODDS_H, M.ODDS_X, M.ODDS_A,M.MATCH_DATE, M.STATUS from MATCH M, Team T1, Team T2 where M.STATUS = 'scheduled' and T1.TEAM_ID = M.HOME and T2.TEAM_ID = M.AWAY", conn);
            //commandMatch.Parameters.AddWithValue("@0", readT.GetInt32(1));
            SqlDataReader reader = commandMatch.ExecuteReader();
            while (reader.Read())
            {
                int ID = reader.GetInt32(0);
                string home = reader.GetString(1);
                string away = reader.GetString(2);
                //Debug.WriteLine(reader.GetDouble(3));
                float odds_h = (float)reader.GetDouble(3);
                float odds_x = (float)reader.GetDouble(4);
                float odds_a = (float)reader.GetDouble(5);
                Match m = new Match(ID, home, away , odds_h , odds_x ,odds_a, reader.GetDateTime(6), reader.GetString(7));
                //m.pick = readT.GetInt32(2);
                matches.Add(m);
            }

            return matches;
        }

        public  static List<Match> getMatchesForTicket(SqlConnection conn, int ticketID)
        {
            SqlCommand commandTeam = new SqlCommand("SELECT * FROM TEAM", conn);
            SqlDataReader read = commandTeam.ExecuteReader();
            Dictionary<int, string> teams = new Dictionary<int, string>();
            while (read.Read())
            {
                teams.Add(read.GetInt32(0), read.GetString(1));
            }


            SqlCommand command = new SqlCommand("SELECT * FROM MATCH_TICKET WHERE TICKET_ID = @0", conn);
            // Add the parameters.
            command.Parameters.Add(new SqlParameter("0", ticketID));
            SqlDataReader readT = command.ExecuteReader();
            List<Match> matches = new List<Match>();

            while (readT.Read())
            {
                SqlCommand commandMatch = new SqlCommand("SELECT * FROM MATCH WHERE MATCH_ID = @0", conn);
                commandMatch.Parameters.AddWithValue("@0", readT.GetInt32(1));
                SqlDataReader reader = commandMatch.ExecuteReader();
                while (reader.Read())
                {
                    int home = reader.GetInt32(1);
                    int away = reader.GetInt32(2);
                    DateTime tmp = reader.GetDateTime(3);
                    Match m = new Match(reader.GetInt32(0), teams[home], teams[away], reader.GetInt32(7), reader.GetInt32(8), tmp, reader.GetString(9));
                    m.pick = readT.GetInt32(2);
                    matches.Add(m);
                }
            }

            return matches;
        }

        public static void updateMatch(Match match, SqlConnection conn)
        {
            //se koristi za update na natprevarite koi se igraat momentalno
            SqlCommand command = new SqlCommand("UPDATE MATCH SET GOALS_H=@homeGoals, GOALS_A=@awayGoals, STATUS=@status  WHERE MATCH_ID = @idMatch", conn);
            command.Parameters.AddWithValue("homeGoals", match.homeGoals);
            command.Parameters.AddWithValue("awayGoals", match.awayGoals);
            command.Parameters.AddWithValue("status", match.status);
            command.Parameters.AddWithValue("idMatch", match.matchID);
            command.ExecuteNonQuery();
        }

        public static List<Match> selectUnfinishedMatch(SqlConnection conn)
        {
            //se koristi za zemanje na natprevari koi ne se zavrsheni
            SqlCommand commandTeam = new SqlCommand("SELECT * FROM TEAM", conn);
            SqlDataReader read = commandTeam.ExecuteReader();
            Dictionary<int, string> teams = new Dictionary<int, string>();
            while (read.Read())
            {
                teams.Add(read.GetInt32(0), read.GetString(1));
            }
            SqlCommand commandMatch = new SqlCommand("SELECT * FROM MATCH WHERE STATUS != 'closed'", conn);
            SqlDataReader reader = commandMatch.ExecuteReader();
            List<Match> matches = new List<Match>();
            while (reader.Read())
            {
                int home = reader.GetInt32(1);
                int away = reader.GetInt32(2);
                DateTime tmp = reader.GetDateTime(3);
                matches.Add(new Match(reader.GetInt32(0), teams[home], teams[away], reader.GetInt32(7), reader.GetInt32(8), tmp, reader.GetString(9)));
            }
            return matches;
        }

        public static List<Match> selectDefaultPageMatch(SqlConnection conn)
        {
            //se koristi za zemanje na natprevari za na pocetnata strana
            SqlCommand commandTeam = new SqlCommand("SELECT * FROM TEAM", conn);
            SqlDataReader read = commandTeam.ExecuteReader();
            Dictionary<int, string> teams = new Dictionary<int, string>();
            while (read.Read())
            {
                teams.Add(read.GetInt32(0), read.GetString(1));
            }
            CultureInfo enUS = CultureInfo.CreateSpecificCulture("en-US");
            SqlCommand commandMatch = new SqlCommand("SELECT * FROM MATCH WHERE convert(varchar, MATCH_DATE, 111) >= '" + String.Format(enUS, "{0 :yyyy/MM/dd}", DateTime.Today) + "'", conn);
            SqlDataReader reader = commandMatch.ExecuteReader();
            List<Match> matches = new List<Match>();
            while (reader.Read())
            {
                int home = reader.GetInt32(1);
                int away = reader.GetInt32(2);
                DateTime tmp = reader.GetDateTime(3);
                matches.Add(new Match(reader.GetInt32(0), teams[home], teams[away], reader.GetInt32(7), reader.GetInt32(8), tmp, reader.GetString(9)));
            }
            return matches;
        }

        public static List<Match> selectBettingMatch(SqlConnection conn)
        {
            //se koristi za zemanje na natprevari na koi moze da se oblozuva
            SqlCommand commandTeam = new SqlCommand("SELECT * FROM TEAM", conn);
            SqlDataReader read = commandTeam.ExecuteReader();
            Dictionary<int, string> teams = new Dictionary<int, string>();
            while (read.Read())
            {
                teams.Add(read.GetInt32(0), read.GetString(1));
            }
            CultureInfo enUS = CultureInfo.CreateSpecificCulture("en-US");
            SqlCommand commandMatch = new SqlCommand("SELECT * FROM MATCH WHERE STATUS = 'scheduled'", conn);
            SqlDataReader reader = commandMatch.ExecuteReader();
            List<Match> matches = new List<Match>();
            while (reader.Read())
            {
                int home = reader.GetInt32(1);
                int away = reader.GetInt32(2);
                DateTime tmp = reader.GetDateTime(3);
                matches.Add(new Match(reader.GetInt32(0), teams[home], teams[away], reader.GetInt32(7), reader.GetInt32(8), tmp, reader.GetString(9)));
            }
            return matches;
        }

        public static List<Match> selectMatch(SqlConnection conn, int matchID)
        {
            //zemanje informacii za samo eden natprevar
            SqlCommand commandTeam = new SqlCommand("SELECT * FROM TEAM", conn);
            SqlDataReader read = commandTeam.ExecuteReader();
            Dictionary<int, string> teams = new Dictionary<int, string>();
            while (read.Read())
            {
                teams.Add(read.GetInt32(0), read.GetString(1));
            }
            SqlCommand commandMatch = new SqlCommand("SELECT * FROM MATCH WHERE MATCH_ID = "+matchID, conn);
            SqlDataReader reader = commandMatch.ExecuteReader();
            List<Match> matches = new List<Match>();
            while (reader.Read())
            {
                int home = reader.GetInt32(1);
                int away = reader.GetInt32(2);
                DateTime tmp = reader.GetDateTime(3);
                matches.Add(new Match(reader.GetInt32(0), teams[home], teams[away], reader.GetInt32(7), reader.GetInt32(8), tmp, reader.GetString(9)));
            }
            return matches;
        }

        public static List<Ticket> getTicketsForDate(DateTime date, SqlConnection conn)
        {
            CultureInfo enUS = CultureInfo.CreateSpecificCulture("en-US");
            SqlCommand command = new SqlCommand("SELECT * FROM TICKET WHERE convert(varchar, TICKET_DATE, 111) = '" + String.Format(enUS, "{0 :yyyy/MM/dd}", date) + "'", conn);
            SqlDataReader reader = command.ExecuteReader();
            List<Ticket> tickets = new List<Ticket>();
            while (reader.Read())
            {
                tickets.Add(new Ticket(reader.GetInt32(0), reader.GetInt32(2), (float)reader.GetDouble(3), reader.GetDateTime(4)));
            }
            return tickets;
        }

        public static void updateBalance(SqlConnection conn, int newBalance, string username)
        {
            SqlCommand command = new SqlCommand("UPDATE USERS SET BALANCE=@newbalance WHERE USER_NAME = @username", conn);
            command.Parameters.AddWithValue("newbalance", newBalance);
            command.Parameters.AddWithValue("username", username);
            command.ExecuteNonQuery();

        }


        public static void updatePaidStatus(SqlConnection conn, int ticketID)
        {
            SqlCommand command = new SqlCommand("UPDATE TICKET SET ISPAID=@1 WHERE TICKET_ID = @2", conn);
            command.Parameters.AddWithValue("1", "yes");
            command.Parameters.AddWithValue("2", ticketID);
            command.ExecuteNonQuery();

        }

        public static bool isDayAdded(SqlConnection conn, DateTime date)
        {
            //proverka dali natprevarite za toj den se dodadeni vo bazata
            CultureInfo enUS = CultureInfo.CreateSpecificCulture("en-US");
            SqlCommand commandTeam = new SqlCommand("SELECT COUNT(*) FROM MATCH WHERE convert(varchar, dateadd(hour, -2, MATCH_DATE), 111) = '" + String.Format(enUS, "{0 :yyyy/MM/dd}", date) + "'", conn);
            var count = commandTeam.ExecuteScalar();
            if (count.ToString().Equals("0"))
            {
                return false;
            }
            else
            {
                return true;
            }
            
        }

        public static bool isUsernameTaken(SqlConnection conn, string username)
        {
            SqlCommand commandTeam = new SqlCommand("SELECT COUNT(*) FROM USERS WHERE USER_NAME = @0", conn);
            commandTeam.Parameters.AddWithValue("0", username);
            var count = commandTeam.ExecuteScalar();
            if (count.ToString().Equals("0"))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static User getUser(SqlConnection conn, string username)
        {
            SqlCommand commandTeam = new SqlCommand("SELECT * FROM USERS WHERE USER_NAME = @0", conn);
            commandTeam.Parameters.AddWithValue("0", username);
            SqlDataReader reader = commandTeam.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                return new User(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetInt32(5), reader.GetString(7));
            }
            else
            {
                return null;
            }
            
        }

        public static bool isTicketPaid(SqlConnection conn, int ticketid)
        {
            SqlCommand commandTeam = new SqlCommand("SELECT * FROM TICKET WHERE TICKET_ID = @0", conn);
            commandTeam.Parameters.AddWithValue("0", ticketid);
            SqlDataReader reader = commandTeam.ExecuteReader();
            reader.Read();
            if (reader.GetString(5).Equals("yes"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}