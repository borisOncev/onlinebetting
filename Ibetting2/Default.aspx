﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="Ibetting2._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    
    
    
    
    
    
    
    
    
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand " href="#">iBetting</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" style="margin-top:10px">
            <asp:Panel ID="Panel1" runat="server" DefaultButton="Button1">
          <ul class="nav navbar-nav navbar-right">
              <li><asp:Label ID="Label1" runat="server" Text="Wrong username or password" Visible="False" ForeColor="Red"></asp:Label></li>
              <li>&nbsp&nbsp</li>
            <li><asp:Button ID="Button3" runat="server" Text="Register" CssClass="btn btn-primary" OnClick="Button3_Click" /></li>
             <li>&nbsp&nbsp</li>
              <li><asp:TextBox ID="TextBox1" runat="server"  CssClass="form-control" placeholder="Username"></asp:TextBox></li>
            <li>&nbsp&nbsp</li>
             <li><asp:TextBox ID="TextBox4" runat="server"  CssClass="form-control" placeholder="Password" TextMode="Password"></asp:TextBox></li>
             <li>&nbsp&nbsp</li>
             <li class="active"><asp:Button ID="Button1" runat="server" Text="Login" CssClass="btn btn-success" OnClick="Button1_Click" /></li>
          </ul>
                </asp:Panel>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

            
            <!--<div class="clear hideSkiplink">
                <asp:Menu ID="NavigationMenu" runat="server" CssClass="menu" EnableViewState="false" IncludeStyleBlock="false" Orientation="Horizontal">
                    <Items>
                        <asp:MenuItem NavigateUrl="~/Default.aspx" Text="Home"/>
                        <asp:MenuItem NavigateUrl="~/About.aspx" Text="About"/>
                    </Items>
                </asp:Menu>
            </div>-->

            

    <h2 align="center">
        Welcome to ibetting!
    </h2>
    <h3 align="center">
        Matches available for betting:
    </h3>
    <div class="tabela">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Timer ID="Timer1"  runat="server" Interval="5000" OnTick="Timer1_Tick" ></asp:Timer>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
                <asp:GridView ID="GridView1" runat="server" HorizontalAlign="Center" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                    <Columns>
                        <asp:BoundField DataField="startTime" HeaderText="Start Time" />
                        <asp:BoundField DataField="home" HeaderText="Home" >
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="homeGoals" HeaderText="Home Goals" >
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="awayGoals" HeaderText="Away Goals" >
                        <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="away" HeaderText="Away" />
                        <asp:BoundField DataField="status" HeaderText="Status" />
                    </Columns>
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                </asp:GridView>
                <br />
                <br />
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <br />
        
        </div>

</asp:Content>
