﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ibetting2
{
    [Serializable]
    public class TMatch
    {
        public int matchID { get; set; }
        public string home { get; set; }
        public string away { get; set; }
        public DateTime startTime { get; set; }
        public int pick { get; set; }
        public float odds { get; set; }


        public TMatch(int M_ID, string Home, string Away, DateTime start, int p, float odds)
        {
            this.matchID = M_ID;
            this.home = Home;
            this.away = Away;
            this.startTime = start;
            this.pick = p;
            this.odds = odds;
        }
    }
}