﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Timers;
using System.Diagnostics;

namespace Ibetting2
{
    public class Global : System.Web.HttpApplication
    {
        

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            SqlConnection conn = new SqlConnection();
            
            // Create the connectionString
            // Trusted_Connection is used to denote the connection uses Windows Authentication
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["betConnection"].ConnectionString;
            conn.Open();

            List<Match> matches = DB.selectUnfinishedMatch(conn);
            Application["matches"] = matches;
            conn.Close();

            Timer t = new Timer();
            t.Interval = 60000;
            t.Elapsed+=t_Elapsed;
            t.Start();
            //DB.insertUser("BorisO", "adminadmin", "Boris", "Oncev", 5000, "boris_O@hotmail.com", conn);
            
        }

        private void t_Elapsed(object sender, ElapsedEventArgs e)
        {
            List<Match> apiMatches = APIMethods.getMatchesForDate(DateTime.Today);
            List<Match> matches = (List<Match>)Application["matches"];

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["betConnection"].ConnectionString;
            conn.Open();

            foreach (Match m in apiMatches)
            {
                if (!m.status.Equals("scheduled"))
                {
                    foreach (Match mat in matches)
                    {
                        if (m.home.Equals(mat.home))
                        {
                            m.matchID = mat.matchID;
                            DB.updateMatch(m, conn);
                            if (m.status.Equals("closed"))
                            {
                                matches.Remove(mat);
                            }
                            break;
                        }
                    }
                }
            }
            conn.Close();
            Application["matches"] = matches;
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown
        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

    }
}
