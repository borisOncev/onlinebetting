﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminAddGames.aspx.cs" Inherits="Ibetting2.AdminAddGames" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand " href="#">iBetting</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" style="margin-top:10px">
          <ul class="nav navbar-nav">
      <li>&nbsp&nbsp&nbsp&nbsp&nbsp</li>
            <li ><a href="AdminTickets.aspx">All Tickets</a></li>
            <li><a href="AdminStats.aspx">Statistics</a></li>
            <li class="active"><a href="AdminAddGames.aspx">Add Games</a></li>
            
          </ul>   
          <ul class="nav navbar-nav navbar-right">
              <li><asp:Label ID="Label2" runat="server" Text="Logged in as: " ></asp:Label></li>
              <li>&nbsp</li>
              <li><asp:Label ID="Label1" runat="server" Text="" ForeColor="#0033CC" ></asp:Label></li>
              <li>&nbsp&nbsp</li>
             <li class="active"><asp:Button ID="Button4" runat="server" Text="Logout" CssClass="btn btn-success" OnClick="Button4_Click" /></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <h1>Add Games</h1>
            <div class="tabela">
        <table style="width:100%;">
            <tr>
                <td style="border-style: solid; border-width: thin">
                    <asp:GridView ID="GridView1" runat="server" HorizontalAlign="Center" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                        <Columns>
                            <asp:BoundField DataField="startTime" HeaderText="Start Time" />
                            <asp:BoundField DataField="home" HeaderText="Home" />
                            <asp:TemplateField HeaderText="1"><ItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox></ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="X"><ItemTemplate><asp:TextBox ID="TextBox2" runat="server"></asp:TextBox></ItemTemplate></asp:TemplateField>
                            <asp:TemplateField HeaderText="2"><ItemTemplate><asp:TextBox ID="TextBox3" runat="server"></asp:TextBox></ItemTemplate></asp:TemplateField>
                            <asp:BoundField DataField="away" HeaderText="Away" />
                            <asp:BoundField DataField="homeGoals" HeaderText="Home Goals" />
                            <asp:BoundField DataField="awayGoals" HeaderText="Away Goals" />
                            <asp:BoundField DataField="status" HeaderText="Status" />
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                    </asp:GridView>
                    <asp:Label ID="Label4" runat="server"></asp:Label>
                    <br />
                    <asp:Button ID="Button3" runat="server" Text="Add Matches" OnClick="Button3_Click" />
                </td>
            </tr>
        </table>
        </div>
</asp:Content>
