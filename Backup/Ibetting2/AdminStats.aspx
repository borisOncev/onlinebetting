﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminStats.aspx.cs" Inherits="Ibetting2.AdminStats" %>
<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div align="right" style="vertical-align: middle;margin-bottom:10px">
                <asp:Label ID="Label3" runat="server" Text="Logged as: " height="26px"></asp:Label>
                <asp:Button ID="Button2" runat="server" Text="Logout" />
</div>
    <div class="clear hideSkiplink">
                <asp:Menu ID="NavigationMenu" runat="server" CssClass="menu" EnableViewState="false" IncludeStyleBlock="false" Orientation="Horizontal">
                    <Items>
                        <asp:MenuItem NavigateUrl="~/AdminTickets.aspx" Text="All Tickets"/>
                        <asp:MenuItem NavigateUrl="~/AdminStats.aspx" Text="Statistics"/>
                        <asp:MenuItem NavigateUrl="~/AdminAddGames.aspx" Text="Add Games"/>
                    </Items>
                </asp:Menu>
            </div>

            <table style="width:100%;">
            <tr>
                <td style="border-style: solid; border-width: thin">
                    
                    <asp:Calendar ID="Calendar1" runat="server"></asp:Calendar>
                    
                </td>
                <td style="border-style: solid; border-width: thin">
                    
                    <asp:Chart ID="Chart1" runat="server">
                        <series>
                            <asp:Series Name="Series1">
                            </asp:Series>
                        </series>
                        <chartareas>
                            <asp:ChartArea Name="ChartArea1">
                            </asp:ChartArea>
                        </chartareas>
                    </asp:Chart>
                    
                </td>
            </tr>
        </table>
</asp:Content>
