﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminAddGames.aspx.cs" Inherits="Ibetting2.AdminAddGames" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div align="right" style="vertical-align: middle;margin-bottom:10px">
                <asp:Label ID="Label3" runat="server" Text="Logged as: " height="26px"></asp:Label>
                <asp:Button ID="Button2" runat="server" Text="Logout" />
</div>
<div class="clear hideSkiplink">
                <asp:Menu ID="NavigationMenu" runat="server" CssClass="menu" EnableViewState="false" IncludeStyleBlock="false" Orientation="Horizontal">
                    <Items>
                        <asp:MenuItem NavigateUrl="~/AdminTickets.aspx" Text="All Tickets"/>
                        <asp:MenuItem NavigateUrl="~/AdminStats.aspx" Text="Statistics" />
                        <asp:MenuItem NavigateUrl="~/AdminAddGames.aspx" Text="Add Games"/>
                    </Items>
                </asp:Menu>
            </div>
            <div class="tabela">
        <table style="width:100%;">
            <tr>
                <td style="border-style: solid; border-width: thin">
                    <asp:GridView ID="GridView1" runat="server" HorizontalAlign="Center">
                    </asp:GridView>
                </td>
            </tr>
        </table>
        </div>
</asp:Content>
